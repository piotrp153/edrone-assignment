import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getMeals } from './store/middleware';

import './App.scss';

import Meal from './components/Meal/Meal';
import Header from './components/Header/Header';
import Sidebar from './components/Sidebar/Sidebar';

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isSidebarOpen: false,
            searchValue: '',
            filteredMeals: [],
            filters: {},
            itemOpen: false,
            favourites: [],
        }
    }


    async componentDidMount() {
        const { getMeals } = this.props;
        await getMeals('52982');
        
        this.setState({ 
            ...this.state, 
            filteredMeals: [...this.props.meals],
            filters: JSON.parse(JSON.stringify( this.props.filters )),
            favourites: JSON.parse(localStorage.getItem('favourites')) || [],
        });
    }

    isFavourite(id){
        const { favourites } = this.state;

        return favourites.some(favourit => favourit.idMeal === id)
    }
    
    renderMeals(meal){
        return meal.map((item, key) => {
            let isFavourite = false;

            if ( this.isFavourite(item.idMeal) ){
                isFavourite = true;
            }

            return <Meal 
                        key={ key } 
                        meal={ item } 
                        favouritesToggle={ e => this.favouritesToggle(e) } 
                        openList={ () => this.openList() } 
                        isFavourite={ isFavourite } 
                    />
        });
    }

    toggleSidebar() {
        const state = this.state;

        this.setState({ ...state, isSidebarOpen: !state.isSidebarOpen });
    }

    searchMeal(e) {
        const state = this.state;
        let searchValue = e.target.value;

        this.setState({ ...state, searchValue: searchValue});
    }

    openList() {
        const state = this.state;

        this.setState({ ...state, itemOpen: !state.itemOpen });
    }  

    updateFilters(type, value) {
        const { filters } = this.state
        let updatedFilter = { ...filters };

        updatedFilter[type][value] = !filters[type][value];

        this.setState({ ...this.state, filters: updatedFilter });
    };

    filterMeals() {
        const { filters } = this.state,
              { meals } = this.props;

        let filteredMeals =  meals.filter(item => {
            let tagsFilter = false;

            if(item.strTags !== null){
                tagsFilter = item.strTags.split(',').some(tag => ( filters.tags[tag] ))
            }

            return  filters.categories[item.strCategory] ||
                    filters.areas[item.strArea] ||
                    tagsFilter
        });

        this.setState({ ...this.state, filteredMeals: filteredMeals });
    }

    filtering(e) {
        const type = e.target.getAttribute('data-type'),
              value = e.target.getAttribute('value');

            this.updateFilters(type, value);
            this.filterMeals();
    }

    favouritesToggle(meal) {
        let localFavourites = JSON.parse(localStorage.getItem('favourites')) || [];

        if ( this.isFavourite(meal.idMeal) ) {
            const index = localFavourites.map(e => e.idMeal).indexOf(meal.idMeal);

            localFavourites.splice(index, 1);
        } else {
            localFavourites.push(meal);
        }

        localStorage.setItem('favourites', JSON.stringify(localFavourites));
        this.setState({ ...this.state, favourites: localFavourites });
    }

    searchFavouriteMeal(e) {
        this.setState( { ...this.state, filters: this.props.filters, searchValue: e.target.value } );
    }
    
    render() {
        const { isSidebarOpen, filteredMeals, searchValue, itemOpen, favourites, filters } = this.state;
        let searchedMeals = [];

        if(filteredMeals) {
            searchedMeals = filteredMeals.filter(item => (
                item.strMeal.toLowerCase().includes(searchValue.toLowerCase())
            ));
        }

        return (
            <div className="App">
                <Header 
                    burgerClick={ () => this.toggleSidebar() } 
                    searchValue={ searchValue } 
                    searchMeal={ e => this.searchMeal(e) } 
                    favourites={ favourites } 
                    searchFavouriteMeal={ (e) => this.searchFavouriteMeal(e) }
                />
                {
                    filters ?
                        <Sidebar 
                            isSidebarOpen={ isSidebarOpen } 
                            filters={ filters } 
                            filtering={ e => this.filtering(e) }
                            searchValue={ searchValue } 
                            searchMeal={ e => this.searchMeal(e) } 
                            favourites={ favourites } 
                            searchFavouriteMeal={ (e) => this.searchFavouriteMeal(e) }
                        />
                    : null
                }
                <div className={ `meals ${ itemOpen ? 'meals--open' : '' }` }>
                    <div className="meals__shade"></div>
                    {
                        searchedMeals.length > 0 ? this.renderMeals(searchedMeals) : ''
                    }
                </div>
            </div>
        )
    }
}


const mapStateToProps = state => ({
    meals: state.meals.meals,
    filters: state.meals.filters,
})

const mapDispatchToProps = dispatch => ({
    getMeals: id => dispatch(getMeals(id)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)( App );
