import React from 'react';

import Favourites from '../Favourites/Favourites';
import './Header.scss';

const header = ({ burgerClick, searchMeal, favourites, searchFavouriteMeal, searchValue }) => (
    <div className="header">
        <div className="header__burger" onClick={ burgerClick }>
            <span></span>
            <span></span>
            <span></span>
        </div>
        <input className="header__search search" type="text" placeholder="Search" value={ searchValue } onChange={ e => searchMeal(e) } />
        <Favourites 
            favourites={ favourites } 
            searchFavouriteMeal={ e => searchFavouriteMeal(e) } 
        />
    </div>
)

export default header;