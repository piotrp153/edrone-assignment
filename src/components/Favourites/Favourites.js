import React, { useState } from 'react';

import { ReactComponent as Heart } from '../../assets/heart.svg';

import './Favourites.scss';

const Favourites = ({favourites, searchFavouriteMeal }) => {
    const [openFavourites, setOpenFavourites] = useState(false);
    
    return <div className={`favourites ${ openFavourites ? 'favourites--open' : '' }`}>
                <div className="favourites__button" onClick={ () => { setOpenFavourites(!openFavourites) } }>
                    <p className="button__name">
                        Favourites
                    </p>
                    <Heart className="button__heart"/>
                </div>
                <ul className="favourites__list">
                    { 
                        favourites.map((item, index) => (
                            <li className="list__item" key={ index } >
                                <button value={ item.strMeal } onClick={ e => searchFavouriteMeal(e) }>
                                    <img src={ item.strMealThumb } alt={ item.strMeal } />
                                    { item.strMeal }
                                </button>
                            </li>
                        ))
                    }
                </ul>
            </div>
}

export default Favourites;