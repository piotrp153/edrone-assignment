import React, { useState } from 'react';

import { ReactComponent as Heart } from '../../assets/heart.svg';

import './Meal.scss';

const Meal = ({ meal, favouritesToggle, isFavourite, openList }) => {
    const [itemOpen, setItemOpen] = useState(false);
    let recepie = [];

    for(let i = 1 ; i <= 20; i++){
        let ingredient = 'strIngredient'+i,
        measure = 'strMeasure'+i;
        
        if(meal[ingredient] === '') {
            break;
        }
        
        recepie = { ...recepie, [meal[ingredient]]: meal[measure] }
    }

    return <div className={`meals__item ${ itemOpen ? 'meals__item--open' : '' }`} >
                <div className="item__holder" onClick={(e) => { setItemOpen(!itemOpen); openList(e) } }>
                    <img className="item__image" src={ meal.strMealThumb } alt="thumbnail" />
                    <h3>{ meal.strMeal }</h3>
                    <div className="item__information">
                        <h2>Instructions</h2>
                        <p>{ meal.strInstructions }</p>
                        <h2>Ingredients + Measure</h2>
                        <ul className="information__list">
                            {
                                Object.keys(recepie).map((item, index) => (
                                    <li className="list__item" key={ index }>{ item } - { recepie[item] }</li>
                                ))
                            }
                        </ul>
                    </div>
                </div>
                <button className={`item__favourites ${ isFavourite ? 'item__favourites--active' : '' }`} onClick={ () => favouritesToggle(meal) }>
                    <Heart className="favourites__heart"/>
                </button>
            </div>
}

export default Meal;