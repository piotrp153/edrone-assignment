import React from 'react';

import './Sidebar.scss';

const sidebar = ({ isSidebarOpen, filters, filtering, searchValue, searchMeal, favourites, searchFavouriteMeal }) => (
    <div className={`sidebar ${ isSidebarOpen ? 'sidebar--active' : '' }`}>
        <input className="sidebar__mobile search" type="text" placeholder="Search" value={ searchValue } onChange={ e => searchMeal(e) } />
        {
            Object.keys(filters).map((filter, index) => (
                <div className="sidebar__item" key={ index }>
                    <h2 className="item__name">
                        { filter }
                    </h2>
                    <ul className="item__list">
                        {
                            Object.keys(filters[filter]).map((item, key) => (
                                <li className ={`list__item ${ filters[filter][item] ? 'list__item--active' : '' }`} 
                                    key={ key } 
                                    data-type={ filter } 
                                    value={ item } 
                                    onClick={ e => filtering(e) }>
                                        { item }
                                </li>
                            ))
                        }
                    </ul>
                </div>
            ))
        }
    </div>
)


export default sidebar;