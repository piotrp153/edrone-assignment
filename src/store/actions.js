export const GET_MEALS_SUCCESS = 'GET_MEALS_SUCCESS';
export const GET_FILTERS_SUCCESS = 'GET_FILTERS_SUCCESS';
export const GET_ERROR = 'GET_ERROR';


export function getMealsSuccess(meals) {
    return {
        type: GET_MEALS_SUCCESS,
        payload: meals
    }
}

export function getFilters(filters) {
    return {
        type: GET_FILTERS_SUCCESS,
        payload: filters
    }
}

export function getError(error) {
    return {
        type: GET_ERROR,
        payload: error
    }
}