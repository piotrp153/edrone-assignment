import { getMealsSuccess, getFilters, getError } from './actions';
import axios from 'axios';

export const getMeals = id => async dispatch => {
    try {  
        let meals = [],
            filters = { categories: {}, areas: {}, tags: {}};

        for(let i = 0; i < 10; i++) {
            const res = await axios.get(`https://www.themealdb.com/api/json/v1/1/lookup.php?i=${id - i}`);
            meals = [ ...meals, res.data.meals[0]];

            if(!(Object.keys(filters.categories).some(item => item.hasOwnProperty(res.data.meals[0].strCategory)))){
                filters = { ...filters, categories: { ...filters.categories, [res.data.meals[0].strCategory]: true}}
            }

            if(!(Object.keys(filters.areas).some(item => item.hasOwnProperty(res.data.meals[0].strArea)))){
                filters = { ...filters, areas: { ...filters.areas, [res.data.meals[0].strArea]: true}}
            }

            if(res.data.meals[0].strTags !== null){
                let tag = res.data.meals[0].strTags.split(',');

                if(!(Object.keys(filters.tags).some(item => item.hasOwnProperty(res.data.meals[0].strTag)))){
                    tag.map(item => (
                        filters = { ...filters, tags: { ...filters.tags, [item]: true}}
                    ));
                }
            }
        }
        dispatch(getMealsSuccess(meals));
        dispatch(getFilters(filters));
    } catch (error) {
        console.log(error);
        dispatch(getError(error));
    }
};