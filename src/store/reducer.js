import {GET_MEALS_SUCCESS, GET_FILTERS_SUCCESS, GET_ERROR } from './actions';

export const initialState = {
    meals: [],
    filters: {},
}

export function mealsReducer(state = initialState, action) {
    switch(action.type) {
        case GET_MEALS_SUCCESS:
            return {
                ...state,
                meals: action.payload
            }
        case GET_FILTERS_SUCCESS:
            return {
                ...state,
                filters: action.payload,
            }
        case GET_ERROR:
            return {
                ...state,
                error: action.payload,
            }
        default: 
            return state;
    }
}